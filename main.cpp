﻿#include "mainwindow.h"
#include "Utils/Request.h"
#include "Utils/ComMessageBox.h"
#include <QApplication>
#include <QSharedMemory>
#include <QFile>
#include <windows.h>


LONG ApplicationCrashHandler(EXCEPTION_POINTERS *pException){
    Q_UNUSED(pException);
    Request request;
    request.reportCrash();
    ComMessageBox::error(NULL,"很抱歉，软件发生了崩溃，请重新启动");
    return EXCEPTION_EXECUTE_HANDLER;
}

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("any12345");
    QCoreApplication::setOrganizationDomain("www.any12345.com");
    QCoreApplication::setApplicationName("DS");
    QCoreApplication::setApplicationVersion("1.0");


    SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)ApplicationCrashHandler);//注冊异常捕获函数

//#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
//    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
//#endif

    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(QStringLiteral(":/res/images/logo.png")));
    QFile qss(":/res/qss/style.qss");
    qss.open(QFile::ReadOnly);
    a.setStyleSheet(qss.readAll());
    qss.close();

    QSharedMemory shared("DS");
    if(shared.attach()){
        ComMessageBox::error(NULL,"软件已经在运行中");
        return 0;
    }
    shared.create(1);

    MainWindow mainWindow;
    mainWindow.show();

    return a.exec();
}
