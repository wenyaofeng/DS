﻿#include "Report.h"
#include "Request.h"
#include <QDebug>



Report::Report(): QThread(nullptr){

    mRequest = new Request(this);

}
Report::~Report(){

    terminate();
    wait();
}
void Report::run(){

    uint32_t reportCount = 0;

    while (true) {
        sleep(120);
        mRequest->reportHeart(reportCount);
        ++reportCount;

    }
}
