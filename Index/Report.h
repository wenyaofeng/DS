﻿#ifndef REPORT_H
#define REPORT_H

#include <QThread>
class Request;
class Report : public QThread
{
    Q_OBJECT
public:
    explicit Report();
    ~Report();
private:
    Request *mRequest;
protected:
    void run() override;
signals:

};

#endif // REPORT_H
