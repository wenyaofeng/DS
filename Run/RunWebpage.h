#ifndef RUNWEBPAGE_H
#define RUNWEBPAGE_H

#include <QWebEnginePage>

class RunWebPage : public QWebEnginePage
{
    Q_OBJECT
public:
    RunWebPage(QWebEngineProfile *profile, QWidget *parent);
    virtual bool certificateError(const QWebEngineCertificateError &error);

signals:
private slots:

};

#endif // RUNWEBPAGE_H
