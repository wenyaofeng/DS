#ifndef REQUEST_H
#define REQUEST_H

#include <QObject>

QT_BEGIN_NAMESPACE;
class QNetworkAccessManager;
QT_END_NAMESPACE;
struct MVersion;

class Request : public QObject
{
    Q_OBJECT
public:
    explicit Request(QObject *parent=nullptr);
private:
    QNetworkAccessManager * createNetworkManager(const QUrl &qurl);
public:
    void reportCrash(); // 上报崩溃信息
    void reportHeart(uint32_t reportCount);// 上传心跳
    void checkVersion();// 检查版本
signals:
    void notifyCheckVersion(bool state,QString &msg,MVersion &version);

};

#endif // REQUEST_H
