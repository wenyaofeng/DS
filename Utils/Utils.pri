HEADERS += \
    $$PWD/ComLineWidget.h \
    $$PWD/ComLoadingLabel.h \
    $$PWD/ComLoadingWidget.h \
    $$PWD/ComMessageBox.h \
    $$PWD/ComSpinWidget.h \
    $$PWD/ComSplitHWidget.h \
    $$PWD/ComSplitVWidget.h \
    $$PWD/database.h \
    $$PWD/Request.h \
    $$PWD/models.h \
    $$PWD/constant.h
	
SOURCES += \
    $$PWD/ComLineWidget.cpp \
    $$PWD/ComLoadingLabel.cpp \
    $$PWD/ComLoadingWidget.cpp \
    $$PWD/ComMessageBox.cpp \
    $$PWD/ComSpinWidget.cpp \
    $$PWD/ComSplitHWidget.cpp \
    $$PWD/ComSplitVWidget.cpp \
    $$PWD/database.cpp \
    $$PWD/Request.cpp \
    $$PWD/models.cpp

INCLUDEPATH += Utils
